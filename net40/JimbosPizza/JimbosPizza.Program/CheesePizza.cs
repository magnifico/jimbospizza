using System;

namespace JimbosPizza
{
    public abstract class CheesePizza : IPizza
    {
        public abstract IPizza CreatePizza();

        public string Recipe
        {
            get
            {
                return "Cheese";
            }
        }
    }
}
