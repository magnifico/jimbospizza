namespace JimbosPizza
{
    public interface IPizzaRecipe
    {
        string Name { get; }

        IPizza CreatePizza();
    }
}