using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace JimbosPizza
{

    [Export(typeof(IPizzaOven))]
    public class RoboPizza5000 : IPizzaOven
    {
        [ImportMany]
        public IEnumerable<IPizzaRecipe> PizzaRecipes { get; set; }

        public IPizza MakePizza(string recipe)
        {
            if (this.PizzaRecipes == null)
            {
                return null;
            }

            var makers = from m in this.PizzaRecipes
                         where string.Compare(m.Name, recipe, StringComparison.OrdinalIgnoreCase) == 0
                         select m;

            var maker = makers.SingleOrDefault();
            if (maker == null)
            {
                return null;
            }

            return maker.CreatePizza();
        }
    }
}