using System;
using System.ComponentModel.Composition;

namespace JimbosPizza
{
    public class DeluxeCheesePizza : CheesePizza
    {
        [Export(typeof(Func<IPizza>))]
        public override IPizza CreatePizza()
        {
            return new DeluxeCheesePizza();
        }

        public override string ToString()
        {
            return "Savory Deluxe Cheese Pizza... Mmm ...";
        }
    }
}