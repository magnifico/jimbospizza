﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Reflection;
using ApprovalTests;
using Microsoft.ComponentModel.Composition.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JimbosPizza.Demo
{
    [TestClass]
    public class IntegrationTest
    {
        private const string BreakPattern = "\r\n\r\n";

        [TestMethod]
        public void DiscoverParts()
        {
            try
            {
                ComposablePartCatalog catalog = Program.Catalog;
                ExportProvider host = Program.Container;
                CompositionInfo info = new CompositionInfo(catalog, host);
                using (StringWriter output = new StringWriter())
                {
                    CompositionInfoTextFormatter.Write(info, output);
                    string[] blocks = output.ToString()
                        .Split(new string[] { BreakPattern }, StringSplitOptions.RemoveEmptyEntries);
                    Array.Sort(blocks);
                    string result = string.Join(BreakPattern, blocks);
                    Approvals.Verify(result);
                }
            }
            catch (ReflectionTypeLoadException ex)
            {
                Array.ForEach(
                    ex.LoaderExceptions,
                    lex => Console.Error.WriteLine(lex.ToString()));
                throw;
            }
        }
    }
}