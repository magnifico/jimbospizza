using System.ComponentModel.Composition;
using System;

namespace JimbosPizza
{
    [Export(typeof(IPizzaStore))]
    public class PizzaStore : IPizzaStore
    {
        [Import]
        public IPizzaOven Oven { get; set; }
    }
}