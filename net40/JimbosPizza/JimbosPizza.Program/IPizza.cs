namespace JimbosPizza
{
    public interface IPizza
    {
        IPizza CreatePizza();
        string Recipe { get; }
    }
}