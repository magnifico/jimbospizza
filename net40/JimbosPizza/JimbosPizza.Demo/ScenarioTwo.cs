using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using ApprovalTests;
using ApprovalTests.Reporters;
using ApprovalUtilities.Utilities;
using Microsoft.ComponentModel.Composition.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JimbosPizza.Demo
{
    [TestClass]
    public class ScenarioTwo
    {
        public static CompositionContainer GetHost()
        {
            var catalog = new TypeCatalog(
                typeof(RoboPizza5000),
                typeof(BasicCheesePizza),
                typeof(CheesePizzaRecipe));
#if !DOTNET45
            return new CompositionContainer(catalog);
#else
            return new CompositionContainer(catalog, CompositionOptions.DisableSilentRejection);
#endif
        }

        [TestMethod]
        [UseReporter(typeof(TortoiseDiffReporter))]
        public void FailToMakeCheesePizza()
        {
            try
            {
                // Still no (visible) exceptions in .net 4... we'll see about .net 4.5...
                var pizzaOven = GetHost().GetExportedValue<IPizzaOven>();
                Approvals.Verify(pizzaOven.MakePizza("Cheese"));
            }
            catch (CompositionException ex)
            {
                Approvals.Verify(ex);
            }
        }

        [TestMethod]
        [UseReporter(typeof(NotepadLauncher))]
        public void BecauseTheCheesePizzaRecipeCannotBeSatisfied()
        {
            try
            {
                // We also see an exception the host can't satisfy the part dependencies.
                GetHost().GetExportedValue<IPizzaRecipe>();
            }
            catch (ImportCardinalityMismatchException ex)
            {
                Approvals.Verify(ex.ToString().ScrubPath(PathUtilities.GetDirectoryForCaller()));
            }
            catch (CompositionException ex)
            {
                Approvals.Verify(ex);
            }
        }

        [TestMethod]
        [UseReporter(typeof(NotepadLauncher))]
        public void ViewTrace()
        {
            try
            {
                GetHost().GetExportedValue<IPizzaRecipe>();
            }
            finally
            {
#if !NCRUNCH
                string location = Assembly.GetExecutingAssembly().Location;
                string log = Path.ChangeExtension(location, "log");
                System.Diagnostics.Process.Start(log);
#endif
            }
        }

        [TestMethod]
        [UseReporter(typeof(NotepadLauncher))]
        public void Diagnose()
        {
            try
            {
                CompositionContainer host = GetHost();
                CompositionInfo info = new CompositionInfo(host.Catalog, host);
                using (StringWriter output = new StringWriter())
                {
                    CompositionInfoTextFormatter.Write(info, output);
                    Approvals.Verify(output);
                }
            }
            catch (ReflectionTypeLoadException ex)
            {
                Array.ForEach(
                    ex.LoaderExceptions,
                    lex => Console.Error.WriteLine(lex.ToString()));
                throw;
            }
        }
    }
}