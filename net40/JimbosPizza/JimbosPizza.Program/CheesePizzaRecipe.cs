using System;
using System.ComponentModel.Composition;

namespace JimbosPizza
{
    [Export(typeof(IPizzaRecipe))]
    public class CheesePizzaRecipe : IPizzaRecipe
    {
        [Import("Cheese")]
        public Func<IPizza> CreateInstance { get; set; }

        public string Name
        {
            get { return "Cheese"; }
        }

        public IPizza CreatePizza()
        {
            return CreateInstance();
        }
    }
}