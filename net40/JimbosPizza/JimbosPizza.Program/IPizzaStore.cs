namespace JimbosPizza
{
    public interface IPizzaStore
    {
        IPizzaOven Oven { get; }
    }
}