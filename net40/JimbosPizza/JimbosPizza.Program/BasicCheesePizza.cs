using System;
using System.ComponentModel.Composition;

namespace JimbosPizza
{
    public class BasicCheesePizza : CheesePizza
    {
        [Import("Cheese", typeof(ITopping))]
        public ITopping Topping { get; set; }

        [Export("Cheese", typeof(Func<IPizza>))]
        public override IPizza CreatePizza()
        {
            return new BasicCheesePizza();
        }

        public override string ToString()
        {
            return "Yummy Cheese Pizza!";
        }
    }
}