using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using ApprovalTests;
using ApprovalTests.Reporters;
using ApprovalUtilities.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JimbosPizza.Demo
{
    [TestClass]
    public class ScenarioOne
    {
        public static CompositionContainer GetHost()
        {
            var catalog = new TypeCatalog(typeof(RoboPizza5000));
#if !DOTNET45
            return new CompositionContainer(catalog);
#else
            return new CompositionContainer(catalog, CompositionOptions.DisableSilentRejection);
#endif
        }

        [TestMethod]
        [UseReporter(typeof(TortoiseDiffReporter))]
        public void FailToMakeCheesePizza()
        {
            // Notice that no exceptions are thrown, the oven only has optional imports.
            CompositionContainer container = GetHost();
            var pizzaOven = container.GetExportedValue<IPizzaOven>();

            Approvals.Verify(pizzaOven.MakePizza("Cheese"));
        }

        [TestMethod]
        [UseReporter(typeof(NotepadLauncher))]
        public void BecauseThereIsNoCheesePizzaRecipe()
        {
            try
            {
                // We see an exception only if we ask for a specific part that the host can't satisfy.
                GetHost().GetExportedValue<IPizzaRecipe>("Cheese");
            }
            catch (ImportCardinalityMismatchException ex)
            {
                Approvals.Verify(ex.ToString().ScrubPath(PathUtilities.GetDirectoryForCaller()));
            }
        }

        [TestMethod]
        [UseReporter(typeof(NotepadLauncher))]
        public void ViewTrace()
        {
            try
            {
                GetHost().GetExportedValue<IPizzaRecipe>("Cheese");
            }
            finally
            {
#if !NCRUNCH
                string location = Assembly.GetExecutingAssembly().Location;
                string log = Path.ChangeExtension(location, "log");
                System.Diagnostics.Process.Start(log);
#endif
            }
        }
    }
}