namespace JimbosPizza
{
    public interface IPizzaOven
    {
        IPizza MakePizza(string recipe);
    }
}