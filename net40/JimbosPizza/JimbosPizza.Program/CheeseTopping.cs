using System.ComponentModel.Composition;

namespace JimbosPizza
{
    [Export("Cheese", typeof(ITopping))]
    public class CheeseTopping : ITopping
    {
        public string ToppingStyle { get { return "Shredded"; } }
    }
}