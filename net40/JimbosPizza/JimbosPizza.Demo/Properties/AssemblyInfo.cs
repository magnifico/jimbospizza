﻿using System.Reflection;
using System.Runtime.InteropServices;
using ApprovalTests.Reporters;

[assembly: UseReporter(typeof(DiffReporter))]
[assembly: AssemblyTitle("JimbosPizza.Demo")]
[assembly: AssemblyCompany("Jim Counts")]
[assembly: AssemblyProduct("JimbosPizza.Demo")]
[assembly: AssemblyCopyright("Copyright © Jim Counts 2012")]
[assembly: ComVisible(false)]
[assembly: Guid("e34f9bcf-bdbd-4110-8d25-f573ebcaa9b1")]
[assembly: AssemblyVersion("1.0.*")]