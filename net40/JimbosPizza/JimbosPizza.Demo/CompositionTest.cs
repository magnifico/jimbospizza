﻿using System.ComponentModel.Composition.Hosting;
using CompositionTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JimbosPizza.Demo
{
    [TestClass]
    public class CompositionTest
    {
        [TestMethod]
        public void VerifyComposition()
        {
#if !DOTNET45
            Composition.DiscoverParts(Program.Catalog, StringExtensions.ScrubPublicKeyToken);
#else
            Composition.DiscoverParts(
                Program.Catalog,
                new CompositionContainer(Program.Catalog, CompositionOptions.DisableSilentRejection),
                StringExtensions.ScrubPublicKeyToken);
#endif
        }
    }
}