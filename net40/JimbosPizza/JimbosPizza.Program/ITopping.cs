﻿namespace JimbosPizza
{
    public interface ITopping
    {
        string ToppingStyle { get; }
    }
}