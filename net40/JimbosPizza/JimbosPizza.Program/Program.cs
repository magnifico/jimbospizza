﻿using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Reflection;

namespace JimbosPizza
{
    public class Program
    {
        static Program()
        {
            Catalog = new AssemblyCatalog(Assembly.GetAssembly(typeof(Program)));
            Container = new CompositionContainer(Catalog);
        }

        public static ComposablePartCatalog Catalog { get; set; }

        public static ExportProvider Container { get; set; }

        private static void Main(string[] args)
        {
        }
    }
}